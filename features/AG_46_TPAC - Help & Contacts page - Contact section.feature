@AG_46 @Regression
Feature:AG_46_TPAC - Help & Contacts page - Contact section
  As a TPAC user I want see the Help & Support Contact details relevant to my location & employer so that I can reach out to them for help.

  @AG_46:1
  Scenario:
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    And User clicks on Hamburger menu link
    And User clicks on Help&Contacts link
    Then User should be on "HelpandContacts_Page|header_Form" page
    And section should include contact details relevant to User's Country
    And section should include contact details relevant to User's Employer

#TPAC Help & Contacts page will adopt similar design as Harmonise
#Contacts section captures TPAC support phone and email address
#TPAC Help & Contacts page including Contacts section should be CMS author able for author to manage per TPAC client