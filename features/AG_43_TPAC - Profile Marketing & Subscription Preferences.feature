@AG_43 @Regression

  #Zeplin-TPAC: Profile Communications
Feature: As an Ageing Works user i want to be able to view and change my marketing and subscription preferences
  @AG_43:1 @smoke
  Scenario: View and Turn off Marketing preferences
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    When User hovers on "Home_Page|lnk_Profile" link
    And User clicks on "Home_Page|lnk_Viewprofile" link
    And User clicks on Communications link
    When I have checked that the "Contact me with news, offers and suitable products" is displayed with a 'yes/no' toggle switch
    And I select toggle to 'No'
    Then all preferences becomes inactive or greyed out

  @AG_43:2
  Scenario: View and Turn on Subscription preferences
    When I have checked that the "Contact me with news, offers and suitable products" is displayed with a 'yes/no' toggle switch
    And I select toggle to 'Yes'
    Then all preferences becomes active

  @AG_43:3
  Scenario: View and Turn off Subscription preferences
    When I have checked that the "Contact me with news, offers and suitable products" is displayed with a 'yes/no' toggle switch
    And I select toggle to 'No'
    Then all preferences becomes inactive or greyed out
