@AG_28 @Regression
Feature:AG_28_TPAC - User Registration - First time journey - T&C
  As a first time user I would like to understand Terms & Conditions (T&C) that apply to my use of and any information I may obtain from TPAC.

  @AG_28:1 @smoke
  Scenario: Default state of Agree & Disagree button
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on Terms&Conditions page
    When User does not check in Accept T&C checkbox
    Then Agree button should be in disabled state by default
    And User should receive tool tip on clicking of disabled Agree button
    And Disagree CTA link should be active by default

  @AG_28:2
  Scenario: User clicks on Accept T&C checkbox

    When User checks in Accept T&C checkbox
    Then Agree button should change to enabled state
    And Disagree CTA link should be active by default

  @AG_28:3
  Scenario: User clicks Disagree

    When User clicks on Disagree to deny T&C
    Then System should launch browser modal with copy
    And modal should include dismiss button to close modal

#User will not be able to proceed without Accepting T&C