/**
 * Created by muthupondi-s on 10/23/2017.
 */


var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given}) {
    Given(/^User should see the following label and link "([^"]*)" "([^"]*)"$/, function (text1, text2, done) {
        keyword.expectVisible('Login_Page|lbl_NewtoAgeingWorks', function () {
            keyword.verifyText('Login_Page|lbl_NewtoAgeingWorks', text1, function () {
                keyword.verifyText('Login_Page|lnk_Registerhere', text2, function () {
                    done();

                });
            });
        });
    }),
        Given(/^User clicks Register Here$/, function (done) {
            keyword.performclick('Login_Page|lnk_Registerhere', function () {
                done();
            });
        });
});