/**
 * Created by muthupondi-s on 11/9/2017.
 */
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given,When,Then,And}) {

    Then(/^User should see "([^"]*)" toggle is "([^"]*)"$/, function (element, data, done) {

        keyword.expectVisible(element, function () {
            keyword.verifyText(element, data, function () {
                done();
            })
        });
    }),
        When(/^User hovers on "([^"]*)" link$/, function (element, done) {
            keyword.mouseOver(element, function () {
                done();
            });

        }),

        Then(/^User should see notificaion message "([^"]*)" "([^"]*)"$/, function (header, text, done) {
            keyword.expectVisible('Home_Page|header_Notification_subc', function () {
                keyword.verifyText('Home_Page|header_Notification_subc', header, function () {
                    keyword.verifyText('Home_Page|msg_Notification_subc', text, function () {
                        done();
                    });
                });

            });
        }),

        Then(/^User scrolls  to the Subscription preference section$/, function (done) {
            keyword.expectVisible('Communications_page|header_Subscriptionpref',function() {
                keyword.scrollToElementByLocatorIntoView('Communications_page|header_Subscriptionpref', function () {
                    //keyword.scrollToelementPosition(10000, function () {
                    done();
                });
            });
        }),

        Then(/^User scrolls  to the Ageingworks logo$/, function (done) {
            browser.sleep(2000);
            browser.executeScript('window.scrollTo(0,0);').then(function () {
                console.log("Scrolled to the given position");
                done();
            });
        });
});