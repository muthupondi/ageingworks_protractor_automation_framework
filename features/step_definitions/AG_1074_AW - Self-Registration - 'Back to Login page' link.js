/**
 * Created by muthupondi-s on 11/9/2017.
 */

/**
 * Created by muthupondi-s on 10/23/2017.
 */


var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given}) {
    Given(/^User should see the label "([^"]*)" and link "([^"]*)" on the "([^"]*)" user registration page$/, function (text1, text2, done) {
        keyword.verifyText('Login_Page|lbl_NewtoAgeingWorks', text1, function () {
            keyword.verifyText('Login_Page|lnk_Login', text2, function () {
                done();

            });
        });
    }),

        Given(/^User is on Self registration page "([^"]*)"$/, function (arg1, done) {
            browser.get(arg1);
            browser.waitForAngularEnabled(false);
            browser.sleep(4000);
            keyword.expectVisible("Registration_page|input_Firstname", function () {
                keyword.expectVisible("Registration_page|link_AGlogo", function () {
                    console.log("TPAC Registration page displayed successfully");
                    done();
                });
            });
        });
});