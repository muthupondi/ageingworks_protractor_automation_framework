
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');
var protractor=require('protractor')


defineSupportCode(function ({Given,Then}) {

    Given(/^User accesses the TPAC registration URL$/, function (done) {
        browser.get(global_url['appURL'].Selfregistration);
        browser.waitForAngularEnabled(false);
        done();
    }),

        Given(/^User lands on Registration page$/, function (done) {
            keyword.expectVisible("Registration_page|input_Firstname", function () {
                keyword.expectVisible("Registration_page|link_AGlogo", function () {
                    console.log("TPAC Registration page displayed successfully");
                    done();
                });
            })
        }),

        Given(/^Check all the fields are empty$/, function (done) {

            keyword.setText('Registration_page|input_Firstname', " ", function () {
                keyword.getInputBoxText('Registration_page|input_Firstname', function (fname) {
                    keyword.isFieldEmpty(fname, "Firstname")
                    keyword.setText('Registration_page|input_Lastname', " ", function () {
                        keyword.getInputBoxText('Registration_page|input_Lastname', function (lname) {
                            keyword.isFieldEmpty(lname, "Lastname")
                            keyword.setText('Registration_page|input_Companykeyword', " ", function () {
                                keyword.getInputBoxText('Registration_page|input_Companykeyword', function (com_key) {
                                    keyword.isFieldEmpty(com_key, "Company Keyword")
                                    keyword.setText('Registration_page|input_Emailaddress', " ", function () {
                                        keyword.getInputBoxText('Registration_page|input_Emailaddress', function (regemail) {
                                            keyword.isFieldEmpty(regemail, "Email address")
                                            keyword.setText('Registration_page|input_RegPassword', " ", function () {
                                                keyword.getInputBoxText('Registration_page|input_RegPassword', function (regpass) {
                                                    keyword.isFieldEmpty(regpass)
                                                    keyword.getInputBoxText('Registration_page|input_RetypeEmailaddress', function (regpass) {
                                                        keyword.isFieldEmpty(regpass)
                                                        done();
                                                    });
                                                });

                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }),


        Given(/^User fills mandatory data "([^"]*)"$/, function (args1, done) {
            console.log(JSON.stringify(args1) + " testing args");
            var reg_user_obj = require("../../testData/registration_user_data.js")[args1]
            keyword.setText('Registration_page|input_Firstname', reg_user_obj.FirstName, function () {
                keyword.setText('Registration_page|input_Lastname', reg_user_obj.LastName, function () {
                    keyword.setText('Registration_page|input_Companykeyword', reg_user_obj.Company_Keyword, function () {
                        keyword.setText('Registration_page|input_Emailaddress', reg_user_obj.Email_Id, function () {
                            keyword.setText('Registration_page|input_RetypeEmailaddress', reg_user_obj.Retype_Id, function () {
                                keyword.setText('Registration_page|input_RegPassword', reg_user_obj.Password, function () {
                                    keyword.setText('Registration_page|input_RetypePassword', reg_user_obj.Re_Password, function () {
                                        keyword.performclick('Registration_page|btn_Save', function () {
                                            var msg = "Please complete all fields."
                                            keyword.verifyText('Registration_page|msg_Completefields', msg, function () {
                                                console.log("Error message :" + msg + "displayed successfully");
                                                done();

                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }),

        Given('User clicks on Save', function (callback) {
            keyword.performclick('Registration_page|btn_Save', function () {
                done();
            });
        }),


        Given(/^User get error message as "([^"]*)"$/, function (msg, done) {
            keyword.getText('Registration_page|msg_Completefields', function (Errormessage) {
                if (Errormessage.trim() === msg.trim())
                    console.log("Error message :" + Errormessage + "displayed successfully");
                done();
            });
        }),


        Given(/^User clicks on Cancel button$/, function (done) {
            keyword.performclick('Registration_page|btn_Cancel', function (Errormessage) {
                done();
            });
        }),
        Given(/^Login page is displayed$/, function (done) {
            keyword.expectVisible('Login_Page|input_Email', function () {
                keyword.expectVisible('Login_Page|link_AGlogo', function () {
                    done();
                });
            });
        }),


        Given(/^User checks T&C page is displayed$/, function (done) {
            keyword.performclick('Login_Page|btn_Submit', function () {
                done();
            });
        }),

        Given(/^User should see the inputfield "([^"]*)" as blank$/, function (element, done) {

            keyword.isFieldEmpty(element, function () {
                keyword.expectEnabled(element, function () {
                    done();
                });
            })
        }),
        Then(/^User should see "([^"]*)" is prepopulated with value "([^"]*)" and field is non editable$/, function (element, value, done) {
            keyword.expectVisible(element, function () {
                keyword.expectDisabled(element, function () {
                    keyword.verifyInputBoxText(element, value, function () {
                        done();
                    });
                });
            });
        }),
        Then(/^User should see "([^"]*)" is prepopulated with value "([^"]*)" and field is  editable$/, function (element, value, done) {
            keyword.expectVisible(element, function () {
                keyword.expectEnabled(element, function () {
                    keyword.verifyInputBoxText(element, value, function () {
                        done();
                    });
                });
            });
        });
});