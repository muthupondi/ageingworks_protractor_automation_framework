var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given,When,Then,And}) {

    Given(/^User should see link for "([^"]*)" on page footer$/, function (locator, done) {
        keyword.expectVisible(locator, function () {
            done();
        });
    }),

        Given(/^User clicks on "([^"]*)" link$/, function (locator, done) {

            browser.sleep(4000);
            keyword.expectVisible(locator, function () {
                keyword.performclick(locator, function () {
                    done();
                });
            });
        }),
        Given(/^User clicks on Health link$/, function ( done) {

            browser.sleep(10000);
            keyword.expectVisible('Home_Page|lnk_Healthmenu', function () {
                keyword.performclick('Home_Page|lnk_Healthmenu', function () {
                    done();
                });
            });
        }),
        Given(/^User clicks on Communications link$/, function (done) {

            browser.sleep(10000);
            keyword.performclick('Viewprofile_page|lnk_communications', function () {
                done();
            });
        }),

        Given(/^Mouse over$/, function (done) {
            browser.sleep(5000);
            browser.actions().mouseMove(element(by.css('#header_H1M8-ZxNtjg-'))).perform();
            element(by.css('#header_H1M8-ZxNtjg-')).click();
        }),

        Given(/^User should be on "([^"]*)" page$/, function (arg1, done) {
            keyword.expectVisible(arg1, function () {
                done();
            });
        }),
        Given(/^User should be see "([^"]*)" page in a new tab$/, function (arg1, done) {
            browser.sleep(2000);
            browser.getAllWindowHandles().then(function (handles) {
                browser.switchTo().window(handles[1]);
                keyword.expectVisible(arg1, function () {
                    done();
                });
            });
        }),

        Given(/^User scrolls to the "([^"]*)"$/, function (locator,done) {
            browser.sleep(5000);
            keyword.expectVisible(locator, function () {
                keyword.scrollToElementByLocatorIntoView(locator, function () {
                    done();
                });
            });
        });
});
