/**
 * Created by muthupondi-s on 11/22/2017.
 */


var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');

defineSupportCode(function ({Given, When, Then, And}) {
    Then(/^User should see Feedback Form to seek help$/, function (done) {
        keyword.expectVisible('HelpandContacts_Page|header_Form', function () {
            keyword.verifyText('HelpandContacts_Page|header_Form', "NEED HELP OR WANT TO GIVE FEEDBACK", function () {
                done();
            });
        });
    });
    Then(/^Form should include dropdownlist of feedback topics "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)" "([^"]*)"$/, function (a1,a2,a3,a4,a5,a6,done) {
        keyword.getDropdownvalues('HelpandContacts_Page|drp_Feedback', function (list) {

            drpvalues=[a1,a2,a3,a4,a5,a6];
          console.log(drpvalues.equals(list,false));
          if(drpvalues.equals(list,false)!==true){
              throw "Dropdown values mismatch"
          }
            done();
        });

    });



    Array.prototype.equals = function (array, strict) {
        if (!array)
            return false;
        if (arguments.length == 1)
            strict = true;

        if (this.length != array.length)
            return false;

        for (var i = 0; i < this.length; i++) {
            if (this[i] instanceof Array && array[i] instanceof Array) {
                if (!this[i].equals(array[i], strict))
                    return false;
            }
            else if (strict && this[i] != array[i]) {
                return false;
            }
            else if (!strict) {
                return this.sort().equals(array.sort(), true);
            }
        }
        return true;
    }

    Then(/^Form should include text box to describe feedback$/, function (done) {
        done();
    });

    Then(/^Form should include a button to submit feedback$/, function (done) {
        done();
    });


    Then(/^User clicks on Hamburger menu link$/, function (done) {
        browser.sleep(7000);
        keyword.expectVisible("Home_Page|lnk_Menu", function () {
            keyword.performclick("Home_Page|lnk_Menu", function () {
                done();
            });
        })
    });

    Given(/^User has not selected feedback topics from dropdown list$/, function (done) {
        var el = element(by.xpath('//select[@formcontrolname="type"]'))
        var a = browser.executeScript('return document.querySelectorAll("option[selected]")[0].innerText', el);
        console.log("a " + a);
        keyword.verifyInputBoxText('HelpandContacts_Page|drp_Feedback', "Choose one", function () {
            done()
        });
    });

    Given(/^System should validate & prompt User to select a Topic "([^"]*)"$/, function (msg, done) {
        keyword.expectVisible('HelpandContacts_Page|lbl_ErrorItemSelection', function () {
            keyword.verifyText('HelpandContacts_Page|lbl_ErrorItemSelection', msg, function () {

                done();

            });

        });
    });

    Given(/^User clicks on "([^"]*)" button$/, function (locator, done) {

        keyword.scrollToElementByLocatorIntoView(locator, function () {
            keyword.expectVisible(locator, function () {
                keyword.performclick(locator, function () {
                    browser.executeScript("window.scrollTo(0,0)");
                    done();
                });
            });
        });
    });


    Given(/^User has not included text to describe feedback$/, function (done) {
        keyword.isFieldEmpty("HelpandContacts_Page|input_Message", function () {
            done();
        });

    }),


        Given(/^System should validate & prompt User to describe feedback "([^"]*)"$/, function (msg, done) {
            keyword.verifyText('HelpandContacts_Page|lbl_ErrorFeedbackRequired', msg, function () {
                done();
            });
        });


    Given(/^User has selected feedback topic from dropdown list "([^"]*)"$/, function (topic, done) {
        keyword.selectByVisibleText('HelpandContacts_Page|drp_Feedback', topic, function () {

                done();


        });
    });


    Then(/^User should see successful submission confirmation message "([^"]*)"$/, function (msg, done) {



        //keyword.scrollToElementByLocatorIntoView('HelpandContacts_Page|lbl_SuccessMessage', function () {
            //keyword.expectVisible('HelpandContacts_Page|lbl_SuccessMessage', function () {
                keyword.verifyText('HelpandContacts_Page|lbl_SuccessMessage', msg, function () {
                    done();
                //});
            //})
        })


    });


    Given(/^User has included text to describe feedback$/, function (done) {
        keyword.setText('HelpandContacts_Page|input_Message', "test", function () {
            done();
        });

    });
});