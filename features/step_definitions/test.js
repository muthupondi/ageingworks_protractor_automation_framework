
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');
//var keyword = require('./../utilities/keywords.js')
defineSupportCode(function ({Given,When}) {

    Given(/^user is logged into the CB system$/, function (done) {

        browser.get("https://v218-dal-qa-merceros.mercer.com:10481/#/");
        keyword.performclick('Dashboard_Page|Header_Login', function () {
            keyword.expectVisible('Dashboard_Page|userNameMSSO', function () {
                keyword.performclick('Dashboard_Page|userNameMSSO', function () {
                    keyword.expectVisible('Dashboard_Page|userNameMSSO', function () {
                        keyword.setText('Dashboard_Page|userNameMSSO', 'narasimhudu.manthri@mercer.com', function () {
                            keyword.expectVisible('Dashboard_Page|btn_Submit', function () {
                                keyword.performclick('Dashboard_Page|btn_Submit', function () {
                                    console.log("Submit button clicked")
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    }),

        When(/^User gets the promocard count$/, function (done) {
            keyword.getElementsCount('FullArticle_page|lst_Promocard', function (count) {
                console.log(count)
                done();
            });
        });
    });


