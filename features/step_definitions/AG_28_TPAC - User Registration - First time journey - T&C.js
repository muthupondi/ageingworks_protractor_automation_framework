/**
 * Created by muthupondi-s on 11/24/2017.
 */

var  {defineSupportCode}  = require("cucumber");
var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given,When,Then,And}) {


    Given(/^User should be landed on Terms&Conditions page$/, function (done) {
        keyword.expectVisible('TermsAndConditions_Page|header_TermsOfUse', function () {
            keyword.expectVisible('TermsAndConditions_Page|header_General', function () {

                done();
            });
        });
    });
    Given(/^User does not check in Accept T&C checkbox$/, function (done) {
        keyword.scrollToElementByLocatorIntoView('TermsAndConditions_Page|checkbox_AcceptNotChecked', function () {
            keyword.expectVisible('TermsAndConditions_Page|checkbox_AcceptNotChecked', function () {
                done();

            });
        });
    });

    Given(/^Agree button should be in disabled state by default$/, function (done) {
        keyword.expectVisible('TermsAndConditions_Page|btn_Agreedisabled', function () {
            done();
        });
    });
    Given(/^User should receive tool tip on clicking of disabled Agree button$/, function (done) {
        keyword.performclick('TermsAndConditions_Page|btn_Agreedisabled', function () {
            browser.sleep(3000);
            keyword.verifyText('TermsAndConditions_Page|msg_Error', "OOPS! Please accept the Terms of Use. Thank you!", function () {
                done();


            });
        });

    });



    Given(/^User checks in Accept T&C checkbox$/, function (done) {
        keyword.performclick('TermsAndConditions_Page|checkbox_AcceptNotChecked', function () {
            done();
        });
    });
    Given(/^Agree button should change to enabled state$/, function (done) {
        keyword.expectVisible('TermsAndConditions_Page|btn_Agreeenabled', function () {
            done();
        });
    });
    Given(/^Disagree CTA link should be active by default$/, function (done) {
        keyword.expectEnabled('TermsAndConditions_Page|lnk_Disagree', function () {
            done();
        });
    });


    When(/^User clicks on Disagree to deny T&C$/, function (done) {
        keyword.performclick('TermsAndConditions_Page|lnk_Disagree', function () {
            done()
        });
    });
    When(/^System should launch browser modal with copy$/, function (done) {
        keyword.expectVisible('TermsAndConditions_Page|modalheader_TermsOfUse', function () {
            keyword.verifyText('TermsAndConditions_Page|modalheader_TermsOfUse', "TERMS OF USE", function () {
                done()
            });
        });
    });

    When(/^modal should include dismiss button to close modal$/, function (done) {
        keyword.expectVisible('TermsAndConditions_Page|btn_Dismiss',function(){
            keyword.performclick('TermsAndConditions_Page|btn_Dismiss',function(){
                done()
            });
        });
    });

});
