/**
 * Created by muthupondi-s on 11/30/2017.
 */
var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');

defineSupportCode(function ({Given, When, Then, And}) {
    Given(/^section should include contact details relevant to User's Country$/, function (done) {


        keyword.scrollToElementByLocatorIntoView('HelpandContacts_Page|lbl_CountryPhoneNo', function () {
            keyword.expectVisible('HelpandContacts_Page|lbl_CountryPhoneNo', function () {
                keyword.verifyText('HelpandContacts_Page|lbl_CountryPhoneNo', "UK: 0800 000 0000", function () {
                    done();
                });
            });
        })
    }),
        Given(/^section should include contact details relevant to User's Employer$/, function (done) {
                keyword.expectVisible('HelpandContacts_Page|lbl_CountryEmail', function () {
                    keyword.verifyText('HelpandContacts_Page|lbl_CountryEmail', "support@ageingworks.com", function () {
                        done();
                });
            })
        }),
        Given(/^User clicks on Help&Contacts link$/, function (done) {


            keyword.expectVisible('Home_Page|lnk_Menu_Help_Contacts', function () {
                keyword.performclick('Home_Page|lnk_Menu_Help_Contacts', function () {
                    browser.sleep(4000)
                    done();
                });
            });
        });
});