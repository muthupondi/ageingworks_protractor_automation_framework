/**
 * Created by muthupondi-s on 11/15/2017.
 */

var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');
var chai = require('chai');

defineSupportCode(function({Given}) {

    Given(/^User should  see the bookmark icon$/, function (done) {
        browser.sleep(5000);
        //keyword.expectVisible(locator, function () {
            keyword.getText('Articlesandvideos_Page|lbl_Bookmarkcount', function (t) {
                keyword.removebraces(t, function (value) {
                    console.log("value is  " + value);
                    if (value != 0) {
                        element(by.xpath('(//i[@class="hrm-icon hrm-icon--marker_flag"])[1]')).getCssValue('background-color').then(function (color) {
                            keyword.removespecialcharacter(color, function (colorcode) {
                                var colorhexvalue = "#" + colorcode
                                if (colorhexvalue === "#0000")
                                    console.log("working" + colorhexvalue)
                                done();
                            });
                        });
                    } else {

                        element(by.xpath('(//i[@class="hrm-icon hrm-icon--bookmark"])[1]')).getCssValue('color').then(function (color) {
                            keyword.removespecialcharacter(color, function (colorcode) {
                                var colorhexvalue = "#" + colorcode
                                console.log("working......." + colorhexvalue);
                                done();
                            });
                        });

                    }
                });
            });
        });
   // });
});



