/**
 * Created by muthupondi-s on 11/27/2017.
 */
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');
var nameArray=[];

defineSupportCode(function ({Given,When}) {

    When(/^I have checked that the "([^"]*)" is displayed with a 'yes\/no' toggle switch$/, function (data, done) {
        browser.sleep(5000);
        keyword.expectVisible('Communications_page|lbl_Marketingpref', function () {
            keyword.verifyText('Communications_page|lbl_Marketingpref', data, function () {
                done();

            })
        })

    }),
        When(/^I select toggle to 'No'$/, function (done) {
            console.log("Going")
            var EC = protractor.ExpectedConditions;
            var status = "No";
            var ele=element(By.xpath('(//div[@class="small-4 medium-2 large-2 evo-flex-row--center"])[1]'))
            ele.getText().then(function (status) {
                console.log(status);
                if (status === "Yes") {
                    browser.executeScript("arguments[0].scrollIntoView(true);", ele);
                    element(By.xpath('(//div[@class="evo-slide-toggle--slider"])[1]')).click();
                    console.log("Toggle is just set to No")
                    done();
                }
                return status;
            }).then(function (f) {
                if (f === "No") {
                    console.log("Already set as NO")
                    done();
                }
            });
        })


    When(/^all preferences becomes inactive or greyed out$/, function (done) {
        browser.sleep(5000);
        keyword.expectVisible('Communications_page|toggle_Phoneinactive', function () {
            keyword.expectVisible('Communications_page|toggle_Emailinactive', function () {
                keyword.expectVisible('Communications_page|toggle_Postinactive', function () {
                    done();
                });
            });
        });
    });
    When(/^I select toggle to 'Yes'$/, function (done) {
        console.log("Going")
        var EC = protractor.ExpectedConditions;
        var status = "No";
        element(By.xpath('(//div[@class="small-4 medium-2 large-2 evo-flex-row--center"])[1]')).getText().then(function (status) {
            console.log(status);
            if (status === "No") {
                element(By.xpath('(//div[@class="evo-slide-toggle--slider"])[1]')).click();
                console.log("Toggle is just set to YES")
                done();
            }
            return status;
        }).then(function (f) {
            if (f === "No") {
                console.log("Already set as YES")
                done();
            }
        });
    });

    When(/^all preferences becomes active$/, function (done) {
        browser.sleep(5000);
        keyword.expectVisible('Communications_page|toggle_Phoneactive', function () {
            keyword.expectVisible('Communications_page|toggle_Emailactive', function () {
                keyword.expectVisible('Communications_page|toggle_Postactive', function () {
                    done();
                });
            });
        });
    });
    When(/^I am able to switch individual subscription preferences to yes or no$/, function (done) {
        done();

    });
});