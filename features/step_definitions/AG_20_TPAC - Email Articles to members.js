/**
 * Created by muthupondi-s on 11/2/2017.
 */
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given,When,Then,And}) {

    Then(/^User should see the "([^"]*)" toggle is enabled$/, function (arg1, done) {
        keyword.expectEnabled(arg1, function () {
            done();
        });
    }),

        When(/^User clicks on "([^"]*)" toggle to disable$/, function (arg1, done) {
            keyword.performclick(arg1, function () {
                done();
            });
        }),

        Then(/^the pop up display is closed$/, function (done) {
            keyword.expectNotVisible('PersonalInfo_page|lnk_Cancel',function(){
                done();
            });
        });
    });
