/**
 * Created by muthupondi-s on 10/26/2017.
 */


var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function({Given}) {

    Given(/^User scrolls down to the "([^"]*)" button$/, function (locator, done) {
        //browser.sleep(2000);

        keyword.scrollToelement(locator, function () {
            keyword.expectVisible(locator, function () {
                done();
            });
        });
    }),
        Given(/^User scrolls down to the Send icon$/, function (done) {
            browser.sleep(2000);
            keyword.scrollToElementByLocatorIntoView('FullArticle_page|lnk_Send',function(){
                done();
            });
        }),
        Given(/^User select existing member from the dropdown "([^"]*)"$/, function (dpvalue, done) {
            browser.sleep(10000);
            keyword.expectVisible('FullArticle_page|drp_Selectexistingmember', function () {
                keyword.performclick('FullArticle_page|drp_Selectexistingmember', function () {
                    keyword.selectByVisibleText('FullArticle_page|drp_Selectexistingmember', dpvalue, function () {
                        done();
                    });
                });
            });
        });
});