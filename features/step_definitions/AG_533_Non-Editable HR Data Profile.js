/**
 * Created by muthupondi-s on 11/30/2017.
 */


var {defineSupportCode} = require("cucumber");
var global_url = require('../../testdata/global.js');

defineSupportCode(function ({Given, When, Then, And}) {
    Given(/^User should not see Edit link on profile page$/, function (done) {
        browser.sleep(5000);
        keyword.scrollToElementByLocatorIntoView('PersonalInfo_page|header_Profileoverview', function () {
            keyword.expectNotVisible('PersonalInfo_page|lnk_Edit', function () {
                done();
            });
        });
    }),
        Given(/^all of the profile data fields are read-only and non-editable$/, function (done) {

            keyword.verifyText('PersonalInfo_page|txt_FirstName', "Jordan", function () {
                keyword.verifyText('PersonalInfo_page|txt_LastName', "Garrett", function () {
                    keyword.verifyText('PersonalInfo_page|txt_Age', "21", function () {
                        keyword.verifyText('PersonalInfo_page|txt_Sex', "Female", function () {
                            keyword.isFieldEmpty('PersonalInfo_page|txt_Address', function () {
                                keyword.isFieldEmpty('PersonalInfo_page|txt_PostCode', function () {
                                    keyword.isFieldEmpty('PersonalInfo_page|txt_City', function () {
                                        keyword.verifyText('PersonalInfo_page|txt_Email', "awqa101.eptest.admin2@gisqa.mercer.com", function () {
                                            keyword.isFieldEmpty('PersonalInfo_page|txt_Phone', function () {
                                                keyword.isFieldEmpty('PersonalInfo_page|txt_Country', function () {
                                                    keyword.isFieldEmpty('PersonalInfo_page|txt_Mobile', function () {
                                                        done();
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });

                    });
                });
            });
        });
    });




