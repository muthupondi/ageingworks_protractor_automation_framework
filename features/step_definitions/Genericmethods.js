
var  {defineSupportCode}  = require("cucumber");
var data=require('../../testdata/global.js');
var nameArray=[];
var  userDB;
defineSupportCode(function ({Given}) {

    Given(/^User accessed AG Login page with "([^"]*)"$/, function (userType) {
        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            URL = data.urls.AgeingWorks[execEnv];
            console.log(JSON.stringify(data.users.PrimaryUser.QA) +"users ")
            var credentials =data.users.PrimaryUser;
            userDB = credentials[execEnv];
            browser.get(URL);

        }
        else if(execEnv.toUpperCase() == "UAT")
        {
            URL = data.urls.AgeingWorks[execEnv]
            userDB = data.users.PrimaryUser[userType];
            console.log(userDB);
            browser.get(URL);

        }
        else {
            URL = data.urlHarmoniseV2UAT;
            userDB = data.usersUAT_V2[userType]
        }

    });

    Given(/^User accessed AG Login page$/, function (done) {
        console.log("Launching the url")
        if (global_url.Environment === "QA") {
            browser.get('http://usdf23v0218.mrshmc.com:5024/provision/login')
            browser.waitForAngularEnabled(false);
            console.log("QA");
        }
        else if (global_url.Environment === "UAT") {
            browser.get('http://usdf23v0218.mrshmc.com:5024/provision/login')
            browser.waitForAngularEnabled(false);
        }
        done();
    }),

        Given(/^User logs into AgeingWorks "([^"]*)" "([^"]*)"$/, function (username, password, done) {
            keyword.expectVisible('Login_Page|link_AGlogo', function () {
                keyword.expectVisible('Login_Page|input_Email', function () {
                    keyword.setText('Login_Page|input_Email', username, function () {
                        keyword.performclick('Login_Page|btn_Submit', function () {
                            keyword.expectVisible('Login_Page|input_Password', function () {
                                keyword.setText('Login_Page|input_Password', password, function () {
                                    keyword.performclick('Login_Page|btn_Submit', function () {
                                        keyword.expectVisible('Home_Page|lbl_Ageingworks', function () {
                                            keyword.verifyText('Home_Page|lbl_Ageingworks', "AGEINGWORKS™", function () {
                                                browser.sleep(5000);
                                                keyword.expectVisible('Home_Page|btn_Gotoprofile', function () {
                                                    keyword.performclick('Home_Page|btn_Gotoprofile', function () {
                                                        done();
                                                    });
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        }),

        Given(/^User lands on AG Login screen$/, function (done) {
            keyword.expectVisible('Login_Page|link_AGlogo', function () {

                done();
            })
        }),

        Given(/^User is on Registration page$/, function (done) {

            console.log("Launching the url")
            if (global_url.Environment === "QA") {
                browser.get('http://usdf23v0218.mrshmc.com:5024/provision/registration')
                browser.waitForAngularEnabled(false);
                console.log("QA");
            }
            else if (global_url.Environment === "UAT") {
                browser.get('http://usdf23v0218.mrshmc.com:5024/provision/registration')
                browser.waitForAngularEnabled(false);
            }
            done();
        }),

        Given(/^User should be landed on AG Home page/, function (done) {

            keyword.expectVisible('Home_Page|lbl_Ageingworks', function () {
                keyword.expectVisible('Login_Page|link_AGlogo', function () {
                    keyword.verifyText('Home_Page|lbl_Ageingworks', "AGEINGWORKS™", function () {

                        done();
                    });

                });
            });
        }),
        Given(/^User enters username and hit Next$/, function (done) {
            //browser.sleep(5000);
            keyword.expectVisible('Login_Page|input_Email', function () {
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.setText('Login_Page|input_Email', userDB.username, function () {
                        keyword.performclick('Login_Page|btn_Submit', function () {
                            done();
                        });
                    });
                });
            });
        }),

        Given(/^User enters username and hit Next "([^"]*)"$/, function (username, done) {
            //browser.sleep(5000);
            keyword.expectVisible('Login_Page|input_Email', function () {
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.setText('Login_Page|input_Email', username, function () {
                        keyword.performclick('Login_Page|btn_Submit', function () {
                            done();
                        });
                    });
                });
            });
        }),
        Given(/^User enters password and submits$/, function (done) {

            keyword.expectVisible('Login_Page|input_Password', function () {
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.setText('Login_Page|input_Password', userDB.password, function () {
                        keyword.performclick('Login_Page|btn_Submit', function () {
                            done();
                        })
                    });
                });
            })
        }),

        Given(/^User enters password and submits "([^"]*)"$/, function (password, done) {

            keyword.expectVisible('Login_Page|input_Password', function () {
                keyword.expectVisible('Login_Page|btn_Submit', function () {
                    keyword.setText('Login_Page|input_Password', password, function () {
                        keyword.performclick('Login_Page|btn_Submit', function () {
                            done();
                        })
                    });
                });
            })
        }),
        Given(/^User enters "([^"]*)" "([^"]*)" in the input field$/, function (element, text, done) {
            keyword.setText(element, text, function () {
                done();
            });
        }),

        Given(/^User enters emailaddress "([^"]*)" in the input field$/, function (element, done) {
            keyword.getRandom("email", "", function (mail) {
                keyword.setText(element, mail, function () {
                    done();
                });
            });
        }),

        Given(/^User moves to "([^"]*)" element$/, function (arg1, done) {
            keyword.expectVisible(arg1, function () {
                keyword.mouseOver(arg1, function () {
                    done();
                });
            });
        });
    Given(/^User moves to "([^"]*)" element$/, function (locator, done) {
        keyword.scrollToElementByLocatorIntoView('FullArticle_page|lnk_Send', function () {
            done();
        })
    });
    Given(/^User log out of the application$/, function (done) {

        keyword.scrollToElementByLocatorIntoView('Home_Page|lnk_Profile', function () {
            keyword.mouseOver('Home_Page|lnk_Profile', function () {
                keyword.performclick('Home_Page|lnk_Logout', function () {
                    browser.sleep(5000).then(function () {
                        keyword.expectVisible('Login_Page|input_Email', function () {
                            console.log("User has logged out successfully")

                            done();
                        });
                    });
                });
            });
        });
    });
});



function Longwait() {
    browser.sleep(5000);
}

function Shortwait() {
    browser.sleep(2000);
}

module.exports = {
    Longwait: Longwait,
    Shortwait: Shortwait
};

