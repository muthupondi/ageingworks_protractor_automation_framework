/**
 * Created by muthupondi-s on 11/16/2017.
 */
var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');
var app=require('../../testObjects/appWebObjects');
var c=require('chai');


var nameArray=[];
var EC = protractor.ExpectedConditions;


defineSupportCode(function({Given,When,Then,And})
{


    When(/^User should be on ProfileInfo page$/, function (done) {
        // keyword.expectVisible('PersonalInfo_page|header_Profileoverview', function () {
        keyword.expectVisible('PersonalInfo_page|lnk_Adddependant', function (){
            keyword.scrollToElementByLocatorIntoView('PersonalInfo_page|lnk_Adddependant', function () {
                done();
            });
        });
    }),
        When(/^User clicks on Edit link$/, function (done) {

            keyword.elementtobeclickable('PersonalInfo_page|lnk_Edit', function () {
                keyword.performclick('PersonalInfo_page|lnk_Edit', function () {
                    done();
                });
            });
        }),


        When(/^Users sees First_name, last_name, Email_address field for family or friend in pop-up box$/, function (done) {
            keyword.expectVisible('FullArticle_page|input_Firstname', function () {
                keyword.expectVisible('FullArticle_page|input_Lastname', function () {
                    keyword.expectVisible('FullArticle_page|input_Emailaddress', function () {
                        done();
                    });
                });
            });
        }),

        Then(/^User should see a tick box with text "([^"]*)" confirming permission to invite family or friend$/, function (text,done) {
            keyword.expectVisible('PersonalInfo_page|checkbox_Confirminvite', function () {
                keyword.verifyText('PersonalInfo_page|checkbox_Confirminvite', text, function () {
                    done();
                });
            });
        }),
        Given(/^User enters "([^"]*)"  in the input field$/, function (element, done) {
            keyword.getRandom("text",5,function(t) {
                keyword.setText(element, t, function () {
                    nameArray.push(t)
                    done();
                });
            });
        }),
        When(/^Send button should be inactive by default$/, function (done) {
            keyword.expectVisible('FullArticle_page|btn_Sendinactive', function () {
                done();
            });
        }),
        When(/^Invite button should be inactive by default$/, function (done) {
            keyword.expectVisible('PersonalInfo_page|btn_InviteInActive', function () {
                done();
            });
        }),
        When(/^Invite button should be active by default$/, function (done) {
            keyword.expectVisible('PersonalInfo_page|btn_Invite', function () {
                done();
            });
        }),
        When(/^Cancel button should be active by default "([^"]*)"$/, function (locator,done) {
            keyword.expectEnabled(locator, function () {
                done();
            });
        }),
        When(/^User selects tick box confirming permission to invite family or friend$/, function (done) {
            keyword.performclick('PersonalInfo_page|checkbox_Confirminvite', function () {
                done();
            });
        }),
        When(/^Send button should become active$/, function (done) {
            keyword.expectEnabled('FullArticle_page|btn_Send', function () {
                done();
            });
        }),

        When(/^User should see member added to his connection dropdown/, function (done) {
            var locator = app.FullArticle_page.drp_Selectexistingmember.selector;
            var data=nameArray[0]+" "+nameArray[1]
            console.log(data);
            keyword.selectByVisibleText('FullArticle_page|drp_Selectexistingmember',data,function() {
                browser.sleep(4000)
                done();
            });
        }),
        When(/^User should see member added to his connection list$/, function (done) {
            var locator = app.PersonalInfo_page.lst_Members.selector;
            var data=nameArray[0]+" "+nameArray[1]
            console.log(data);
            locator=locator.replace("<<membername>>",data);
            console.log(locator);
            browser.wait(EC.visibilityOf(element(By.xpath(locator)),15000)).then(function(isvisible){
                expect(isvisible)
                nameArray=[];
                done();
            }).catch(function(error){
                console.log("Element not Exists")
                nameArray=[];
                done();
            });

        }),
        When(/^new member should not be added to connection list$/, function (done) {
            var locator = app.PersonalInfo_page.lst_Members.selector;
            var data=nameArray[0]+" "+nameArray[1]

            console.log(data);
            locator=locator.replace("<<membername>>",data);
            console.log(locator);
            var a = browser.wait(EC.invisibilityOf(element(By.xpath(locator))), 20000)
            a.then(function () {
                console.log("Element doesn't Exists"+locator)
                done();
            }).catch(function(err){
                throw("Element  Exists :"+locator)
                done();
            });
        }),
        When(/^Send link should get enabled$/, function (done) {
            browser.sleep(6000);
            //keyword.expectEnabled('FullArticle_page|lnk_Send', function () {
            done();
            //});
        });

});

