/**
 * Created by muthupondi-s on 10/16/2017.
 */

var  {defineSupportCode}  = require("cucumber");
var global_url=require('../../testdata/global.js');

defineSupportCode(function ({Given}) {

    Given(/^User is on Registration page "([^"]*)"$/, function (url, done) {
        browser.get(url);
        browser.waitForAngularEnabled(false);
        browser.sleep(5000);
        done();
    }),

        /*text -> Expected Verbiage to be seen on the page
         element ->WebElement of the verbiage*/


        Given(/^User should see verbiage as "([^"]*)" "([^"]*)"$/, function (text, element, done) {

            var Exp_verbiage = text;
            browser.sleep(7000);
            keyword.expectVisible(element, function () {
                keyword.verifyText(element, Exp_verbiage, function () {
                    done();
                });
            });
        }),

        Given(/^User should see "([^"]*)" from the dropdown$/, function (text, done) {
            //keyword.expectVisible('Registration_page|drp_Genderdefault',function() {
            var el=element(by.xpath('//option[@selected]'))
              /* var a=  browser.executeScript('return document.querySelectorAll("option[selected]")[0].innerText',el);
               console.log("a "+a);*/
                keyword.verifyInputBoxText(el, text, function () {
                    done()

            });
        });
    });