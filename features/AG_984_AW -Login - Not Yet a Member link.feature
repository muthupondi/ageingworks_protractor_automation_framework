@AG_924 @Regression
Feature: Registration link for Non-Members on Login
  @AG_984:1
    Scenario: Show 'Not Yet a Member' link on Login page
    Given User accessed AG Login page
    Then User lands on AG Login screen
    And User should see the following label and link "New to AgeingWorks™?" "Register Here"

  @AG_984:2
  Scenario: User clicks 'Not Yet a Member'
    When User clicks Register Here
    Then User lands on Registration page

 # Design reference: https://app.zeplin.io/project/58c91bcc06ae859621634bba/screen/59e5d6a1962ccccb14e89402




