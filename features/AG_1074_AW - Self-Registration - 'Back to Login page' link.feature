@AG_1074 @Regression
Feature: Include 'Back to Login page' in Self-Registration
  As a already registered User accessing the AgeingWorks registration page I want an easy to
  login without re-registering.

  @AG_1074:1 @smoke
  Scenario Outline: Login link in Registration pages
    Given User is on Registration page "<Reg_URL>"
    Then User should see the label "<Label>" and link "<LinkText>" on the "<Type>" user registration page

    Examples:
      | Reg_URL                                                                                                       | Type          |Label            |  LinkText|
      |  http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=d39b2376-3d50-4aca-b13f-10533653359d  | HR Provisoned |                 |          |
      |  http://usdf23v0218.mrshmc.com:5024/provision/registration                                                    | Self        |Already a Member?|Log in    |
      |  http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=7d1dc41e-15d1-486d-a6c5-250cba3a9ba5  | Dependant     |                 |          |

  @AG_1074:2

  Scenario: Navigation to AgeingWorks Login page for already registered members
    Given User is on Self registration page "http://usdf23v0218.mrshmc.com:5024/provision/registration"
    And User scrolls to the "Registration_page|lnk_Login"
    When User clicks on "Registration_page|lnk_Login" link
    Then User lands on AG Login screen