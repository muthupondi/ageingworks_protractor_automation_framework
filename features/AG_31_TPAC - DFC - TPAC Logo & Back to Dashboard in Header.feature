@AG_31 @Regression
Feature: AG_31_TPAC - DFC - TPAC Logo & Back to Dashboard in Header

  As a User I want to view Digital Filing Cabinet with TPAC branding so that my user experience is same
  throughout the TPAC journey.

  @AG_31:1
  Scenario: Validate DFC link in the Hamburger Menu
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    And User clicks on "Home_Page|lnk_Menu" link
    #And User clicks on "Home_Page|lnk_DFC" link
    And User log out of the application

    #Then User should be see "Footer_Page|lblheader_Privacypolicy" page in a new tab
