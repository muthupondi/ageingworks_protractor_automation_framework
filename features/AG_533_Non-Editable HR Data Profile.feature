@ag_533 @Regression
Feature: AG_533_Non-Editable HR Data Profile

  As a User with Profile data loaded from a HR data file,
  I want my Profile section data fields to be read-only
  so that the fields cannot be edited

  @AG_533:1 @smoke
  Scenario: Profile Overview is non-editable
    Given User accessed AG Login page with "HRProvisionedUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    When User hovers on "Home_Page|lnk_Profile" link
    And User clicks on "Home_Page|lnk_Viewprofile" link
    And User should not see Edit link on profile page
    Then all of the profile data fields are read-only and non-editable
