@AG_41 @Regression
Feature: AG_41_TPAC - Help & Contacts page - Feedback Form

  As a TPAC User I want to contact Mercer TPAC Support team easily via a structured form so that I can get quick response on topic of inquiry/ feedback.

  @AG_41:1 @smoke
  Scenario: Default TPAC Feedback form state
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    And User clicks on Hamburger menu link
    And User clicks on "Home_Page|lnk_Menu_Help_Contacts" link
    Then User should be on "HelpandContacts_Page|header_Form" page
    Then User should see Feedback Form to seek help
    And Form should include dropdownlist of feedback topics "Choose one" "Complaint" "Feedback" "Product Issue" "Security" "Other"


  @AG_41:2
  Scenario: Submitting Form without Topic
    Given User should be on "HelpandContacts_Page|header_Form" page
    #And   User has not selected feedback topics from dropdown list
    When User clicks on "HelpandContacts_Page|btn_SendFeedback" button
    Then System should validate & prompt User to select a Topic "Item selection required."
#Validation message below field: Item selection required.
  @AG_41:3
  Scenario: Submitting Form without Description
    Given User should be on "HelpandContacts_Page|header_Form" page
    And User has not included text to describe feedback
    When User clicks on "HelpandContacts_Page|btn_SendFeedback" button
    Then System should validate & prompt User to describe feedback "Feedback required."

#Validation message below field: Item selection required.
  @AG_41:4
  Scenario: Successful Form submission to ageingworks@mercer.com
    Given User should be on "HelpandContacts_Page|header_Form" page
    And User has selected feedback topic from dropdown list "Complaint"
    And User has included text to describe feedback
    And User clicks on "HelpandContacts_Page|btn_SendFeedback" button
    Then User should see successful submission confirmation message "Thank you for you feedback/query."

#Success Message: Thank you for your feedback/query
#TPAC Support Inbox: ageingworks@mercer.com

