@780 @Regression
Feature: AG_780_AgeingWorks Bookmark Functionality

  As a User I would like to bookmark/save an article so that I can view it at anytime from my AgeingWorks filing cabinet
  @AG_780:1
  Scenario: Viewing bookmark icons
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    And User clicks on "Home_Page|lnk_Healthmenu" link
    Then User should  see the bookmark icon
    #And User should see the bookmark count "(0)" by default "Articlesandvideos_Page|lbl_Bookmarkcount"


  @AG_780:2
  Scenario: User sees permission tick box to invite new connection in Profile
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    When User hovers on "Home_Page|lnk_Profile" link
    When User clicks on "Home_Page|lnk_Viewprofile" link
    Then User should be on ProfileInfo page
    Then User clicks on Edit link