@AG_20 @Regression

Feature: AG_20_TPAC - Email Articles to members

  As a user i would like to email an article as a PDF so that it can be shared to a member from my profile
  @AG_20:1 @smoke
  Scenario: Email article to members including myself
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    And User clicks on Health link
    And User scrolls to the "Health_page|btn_Keepreading"
    And User clicks on "Health_page|btn_Keepreading" link
    And User scrolls to the "FullArticle_page|lnk_Send"
    And User clicks on "FullArticle_page|lnk_Send" link
    Then User should see the "FullArticle_page|toggle_Sendtome" toggle is enabled
    When User select existing member from the dropdown "BkyBS uklfo"
    And User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"

#check toggle is highlighted when click to the right
#ensure 'X' icon is present next to members name in drop down to be able to remove from the list
#check cancel and 'x' icon exists in the pop up display
#add new member text fields
  @AG_20:2
  Scenario: Email article to members excluding myself
    When User clicks on "FullArticle_page|toggle_Sendtome" toggle to disable
    And User select existing member from the dropdown "BkyBS uklfo"
    And User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"


#standard email validation to handle failed scenario
#error message for failed send to address
#Email template from the AgeingWorks, no-reply, delivery failure of email
  @AG_20:3
  Scenario: Cancel or X button closes the email pop up display
    When User clicks on "FullArticle_page|lnk_Cancel" link
    Then the pop up display is closed
    And User clicks on "FullArticle_page|lnk_Send" link
    When User clicks on "FullArticle_page|lnk_Close" link
    Then the pop up display is closed
    And User log out of the application