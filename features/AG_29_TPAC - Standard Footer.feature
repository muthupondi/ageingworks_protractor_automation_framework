@Regression
Feature: AG_29_TPAC - Standard Footer
  @AG_29:1 @smoke
  Scenario: Validate footer links are displayed for TPAC user
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    Then User should see link for "Home_Page|lnk_AcceptableUse" on page footer
    Then User should see link for "Home_Page|lnk_Cookiepolicy" on page footer
    Then User should see link for "Home_Page|lnk_Privacypolicy" on page footer
    Then User should see link for "Home_Page|lnk_TermsofUse" on page footer
    Then User should see link for "Home_Page|lnk_Help_Contacts" on page footer

  @AG_29:2 @smoke
  Scenario: Validate the footer links navigates to appropriate page
    Given User should be landed on AG Home page
    When User scrolls to the "Home_Page|lnk_Privacypolicy"
    When User clicks on "Home_Page|lnk_Privacypolicy" link
    Then User should be see "Footer_Page|lblheader_Privacypolicy" page in a new tab
    When User clicks on "Footer_Page|lnkheader_Cookiepolicy" link
    Then User should be on "Footer_Page|lblheader_Cookiepolicy" page
    When User clicks on "Footer_Page|lnkheader_AcceptableUse" link
    Then User should be on "Footer_Page|lblheader_AcceptableUse" page
    When User clicks on "Footer_Page|lnkheader_TermsofUse" link
    Then User should be on "Footer_Page|lblheader_TermsofUse" page
    When User clicks on "Footer_Page|lnkheader_Help_Contacts" link
    Then User should be on "HelpandContacts_Page|header_Form" page
    And User log out of the application



