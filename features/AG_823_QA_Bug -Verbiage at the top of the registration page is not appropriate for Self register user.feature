@823 @Regression
Feature: AG_823_QA_Bug -Verbiage at the top of the registration page is not appropriate for Self register user

  Verbiage at the top of the registration page is not appropriate for Self register user
  @AG_823:1
  Scenario Outline: Validate registration verbiage for different users self register ,Dependant and HR provisoned
    Given User is on Registration page "<Reg_URL>"
    Then User should see verbiage as "<Verbiage>" "Registration_page|msg_Header"

  Examples:
     | Reg_URL                                                                                                       | Type|Verbiage |
     |  http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=d39b2376-3d50-4aca-b13f-10533653359d  | HR Provisoned | There’s just one more step to activate your account! Your employer has already provided the details we need to identify you. All you need to do is now is enter a few more details below, choose your password, security questions and of course, confirm if you’re not a robot. When you’ve done that, click on ‘save’ and log in!|
     |  http://usdf23v0218.mrshmc.com:5024/provision/registration                                                    | Self| There’s just one more step to activate your account! All you need to do is now is enter a few more details below, choose your password, security questions and of course, confirm if you’re not a robot. When you’ve done that, click on ‘save’ and log in!         |
     |  http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=7d1dc41e-15d1-486d-a6c5-250cba3a9ba5  | Dependant | There’s just one more step to activate your account! Your family member has already provided the details we need to identify you. All you need to do is now is enter a few more details below, choose your password, security questions and of course, confirm if you’re not a robot. When you’ve done that, click on ‘save’ and log in!|
