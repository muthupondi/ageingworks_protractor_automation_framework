Feature: Include 'Back to Login page' in Self-Registration

  As a already registered User accessing the AgeingWorks registration page I want an easy to
  login without re-registering.

@testing1
  Scenario: Login link in Self- Registration page
    Given user is logged into the CB system
    Then the user views any page in the contractor benchmarking system
