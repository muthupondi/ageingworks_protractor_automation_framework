@AG_677 @Regression
Feature: AG_677_Article Send Message
  As a Ageingworks User I would like to have to have a confirmation message displayed when I Email an Article to a Member
  So that I can see that an article email request has been sent.

  @AG_677:1 @smoke
  Scenario: Article sent confirmation message to existing member
    Given User accessed AG Login page
    Then User lands on AG Login screen
    When User enters username and hit Next "test022.muthupondi.s@gisqa.mercer.com"
    And User enters password and submits "Welcome2"
    Then User should be landed on AG Home page
    And User clicks on "Home_Page|lnk_Healthmenu" link
    And User scrolls to the "Health_page|btn_Keepreading"
    And User clicks on "Health_page|btn_Keepreading" link
    And User scrolls down to the Send icon
    And User clicks on "FullArticle_page|lnk_Send" link
    When User select existing member from the dropdown "DOBadmin test"
    And User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent" "FullArticle_page|lbl_Alertmsg"

  @AG_677:2
  Scenario: Article sent confirmation message to new member
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    And User clicks on "Home_Page|lnk_Healthmenu" link
    And User scrolls to the "Health_page|btn_Keepreading"
    And User clicks on "Health_page|btn_Keepreading" link
    And User scrolls down to the Send icon
    And User clicks on "FullArticle_page|lnk_Send" link
    And User clicks on "FullArticle_page|lnk_nonmember" link
    And User enters "FullArticle_page|input_Firstname" "TestFnameQA" in the input field
    And User enters "FullArticle_page|input_Lastname" "TestLnameQA" in the input field
    And User enters "FullArticle_page|input_Emailaddress" "test999@mercer.com" in the input field
    And User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"

