@AG_1016 @Regression
Feature:AG_1016_Email Article - Confirm to Add Family member
  As an AgeingWorks User I want to confirm that I have permission to invite my friends or family members
  to AgeingWorks so that my consent can be tracked for compliance


  @AG_1016:1 @smoke

  Scenario: User sees permission tick box to invite new connection in Profile
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    When User hovers on "Home_Page|lnk_Profile" link
    When User clicks on "Home_Page|lnk_Viewprofile" link
    Then User should be on ProfileInfo page
    When User clicks on "PersonalInfo_page|lnk_Adddependant" link
    And Users sees First_name, last_name, Email_address field for family or friend in pop-up box
    Then User should see a tick box with text "Your connection will also receive an invite to join AgeingWorks™ for free" confirming permission to invite family or friend
    And Invite button should be inactive by default
    And Cancel button should be active by default "PersonalInfo_page|lnk_Cancel"
  @AG_1016:2
  Scenario: User confirms permission tick box to invite new connection from profile page
    And User enters "FullArticle_page|input_Firstname"  in the input field
    And User enters "FullArticle_page|input_Lastname"  in the input field
    And User enters emailaddress "FullArticle_page|input_Emailaddress" in the input field
    And User selects tick box confirming permission to invite family or friend
    Then Invite button should be active by default
    And Cancel button should be active by default "PersonalInfo_page|lnk_Cancel"
  @AG_1016:3
  Scenario: User invites new connection from profile page
    When  User clicks on "PersonalInfo_page|btn_Invite" link
      #Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"
    And User should see member added to his connection list
  @AG_1016:4
  Scenario: User Cancels new connection
    When User clicks on "PersonalInfo_page|lnk_Adddependant" link
    And User enters "FullArticle_page|input_Firstname"  in the input field
    And User enters "FullArticle_page|input_Lastname"  in the input field
    And User enters emailaddress "FullArticle_page|input_Emailaddress" in the input field
    And User selects tick box confirming permission to invite family or friend
    When User clicks on "PersonalInfo_page|lnk_Cancel" link
    Then the pop up display is closed
    And new member should not be added to connection list
    And User scrolls to the "Login_Page|link_AGlogo"
    And User clicks on "Login_Page|link_AGlogo" link

  @AG_1016:5
  Scenario Outline: User sees permission tick box to invite Non-Member on Share Article

    Then User should be landed on AG Home page
    And User clicks on "Home_Page|lnk_Healthmenu" link
    And User scrolls to the "Health_page|btn_Keepreading"
    And User clicks on "Health_page|btn_Keepreading" link
    And User scrolls to the "FullArticle_page|lnk_Send"
    And Send link should get enabled
    And User clicks on "FullArticle_page|lnk_Send" link
    And User clicks on "FullArticle_page|lnk_nonmember" link
    And Users sees First_name, last_name, Email_address field for family or friend in pop-up box
    Then User should see a tick box with text "<text>" confirming permission to invite family or friend
    And Cancel button should be active by default "FullArticle_page|lnk_Cancel"
    And Send button should be inactive by default


    Examples:
      |text|
      | Your connection will also receive an invite to join AgeingWorks for free |


  @AG_1016:6
  Scenario: User confirms permission tick box to share article to non member
    And User enters "FullArticle_page|input_Firstname"  in the input field
    And User enters "FullArticle_page|input_Lastname"  in the input field
    And User enters emailaddress "FullArticle_page|input_Emailaddress" in the input field
    And User selects tick box confirming permission to invite family or friend
    Then Send button should become active
    And Cancel button should be active by default "FullArticle_page|lnk_Cancel"

  @AG_1016:7
  Scenario: User shares article and should be added in the members list dropdown
    When  User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"
    And User scrolls to the "FullArticle_page|lnk_member"
    And User clicks on "FullArticle_page|lnk_member" link
    And User should see member added to his connection list
    And  User clicks on "FullArticle_page|btn_Send" link
    Then User should see verbiage as "Your Article has been sent." "FullArticle_page|lbl_Alertmsg"



  #Then User logs into AgeingWorks "test005.muthupondi.s@gisqa.mercer.com" "Welcome1"




  Scenario: User Cancels new connection
    And User clicks on "FullArticle_page|lnk_nonmember" link
    And User enters "FullArticle_page|input_Firstname" "TestFnameQA" in the input field
    And User enters "FullArticle_page|input_Lastname" "TestLnameQA" in the input field
    And User enters "FullArticle_page|input_Emailaddress" "test999@mercer.com" in the input field
    When User clicks on "FullArticle_page|lnk_Close" link
    Then the pop up display is closed
    And new member should not be added to connection list

