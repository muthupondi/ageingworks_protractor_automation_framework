@AG_988 @Regression
Feature:
  As a User I would like to be reminded to subscribe to the AgeingWorks newsletter so that I can receive the newsletter every month.




  @AG_988:1 @smoke
  Scenario Outline: : User did not subscribe to AgeingWorks newsletter in Profile
    Given User accessed AG Login page with "PrimaryUser"
    Then User lands on AG Login screen
    When User enters username and hit Next
    And User enters password and submits
    Then User should be landed on AG Home page
    When User hovers on "Home_Page|lnk_Profile" link
    And User clicks on "Home_Page|lnk_Viewprofile" link
    And User clicks on Communications link
    And User scrolls  to the Subscription preference section
    Then User should be on "Communications_page|header_Subscriptionpref" page
    And I select toggle to 'No'
    And User should see "Communications_page|status_subscriptionpref" toggle is "No"
    And User scrolls  to the Ageingworks logo
    When User clicks on "Login_Page|link_AGlogo" link
    Then User should see notificaion message "<Notification_header>" "<Notification_message>"
    Examples:
      | Notification_header             |Notification_message|
      | Get the most from AgeingWorks™ | To get the most from AgeingWorks™ sign up to our monthly newsletter|

  @AG_988:2
  Scenario: User subscribes to AgeingWorks newsletter in Profile
    When User has subscribed to AgeingWorks newsletter in Profile
    Then System should show a Congratulatory message for update
    And System should not show related action notifications on dashboard