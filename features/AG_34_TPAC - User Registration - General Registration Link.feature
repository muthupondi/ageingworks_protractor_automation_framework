@AG_24 @Regression
Feature: AG_34_TPAC - User Registration - General Registration Link
  As a user I want to check login functionality in MFW application

  @AG_24:1
  Scenario: User accesses and views TPAC Registration page
    Given User is on Registration page
    Then User lands on Registration page

  @AG_24:2
  Scenario Outline:Mandatory fields validation in registration page
    Given User is on Registration page
    Then User lands on Registration page
    When User fills mandatory data "<Reg_User>"

    Examples:

      | Reg_User                    |
      | registration_user1          |
      | registration_user2          |
      | registration_user3          |
      | registration_user4          |
      | registration_user5          |
      | registration_user6          |
      | registration_user7          |


  @AG_24:3 @smoke
  Scenario: Validate all the fields are blank by default when user is in self registration page
    Given User is on Registration page
    Then User lands on Registration page
    And  User should see the inputfield "Registration_page|input_Firstname" as blank
    And  User should see the inputfield "Registration_page|input_Lastname" as blank
    And  User should see the inputfield "Registration_page|input_Companykeyword" as blank
    And  User should see the inputfield "Registration_page|input_Emailaddress" as blank
    And  User should see the inputfield "Registration_page|input_RetypeEmailaddress" as blank
    And  User should see the inputfield "Registration_page|input_RegPassword" as blank
    And  User should see the inputfield "Registration_page|input_RetypePassword" as blank

  @AG_24:4 @smoke
  Scenario Outline: Validate the field values are pre-populated on registration page for "<UserType>" user
    Given User is on Registration page "<Reg_url>"
    Then User should see "Registration_page|input_Firstname" is prepopulated with value "<Firstname>" and field is non editable
    Then User should see "Registration_page|input_Lastname" is prepopulated with value "<Lastname>" and field is non editable
    Then User should see "Registration_page|input_Emailaddress" is prepopulated with value "<Email_Address>" and field is non editable
    Then User should see "Registration_page|input_RetypeEmailaddress" is prepopulated with value "<Re-Type Email Address>" and field is non editable
    Then User should see the inputfield "Registration_page|input_RegPassword" as blank
    Then User should see the inputfield "Registration_page|input_RegPassword" as blank
    Then User should see "<Gender>" from the dropdown

    Examples:
      |Reg_url                                                                                                    | Firstname  |Lastname|Day|Month|Year|Gender             |Email_Address                         |Re-Type Email Address                 |Password|Re-Type Password|UserType|
      |http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=e9e23eca-b1e3-47b5-99a0-6fec372975f4| Brandy     |Wood    |   |     |    |  Select Gender    |bb1qa15.eptest.admin2@gisqa.mercer.com|bb1qa15.eptest.admin2@gisqa.mercer.com|        |                |Broker  |

  @AG_24:5
  Scenario Outline: Validate the field values are pre-populated on registration page for "<UserType>" user
    Given User is on Registration page "<Reg_url>"
    Then User should see "Registration_page|input_Firstname" is prepopulated with value "<Firstname>" and field is  editable
    Then User should see "Registration_page|input_Lastname" is prepopulated with value "<Lastname>" and field is  editable
    Then User should see "Registration_page|input_Emailaddress" is prepopulated with value "<Email_Address>" and field is non editable
    Then User should see "Registration_page|input_RetypeEmailaddress" is prepopulated with value "<Re-Type Email Address>" and field is non editable
    Then User should see the inputfield "Registration_page|input_RegPassword" as blank
    Then User should see the inputfield "Registration_page|input_RegPassword" as blank
    Then User should see "<Gender>" from the dropdown


    Examples:
      |Reg_url                                                                                                     | Firstname  |Lastname|Day|Month|Year|  Gender           |Email_Address                         |Re-Type Email Address                 |Password|Re-Type Password|UserType|
      |http://usdf23v0218.mrshmc.com:5024/provision/registration?activationid=7d1dc41e-15d1-486d-a6c5-250cba3a9ba5 | verbtest    |test    |   |     |    |  Select Gender    |test027.muthupondi.s@gisqa.mercer.com|test027.muthupondi.s@gisqa.mercer.com|        |                 |Dependant  |