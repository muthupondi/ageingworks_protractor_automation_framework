var { defineSupportCode } = require('cucumber');

defineSupportCode(function ({ registerHandler }) {
    registerHandler("BeforeFeature", { timeout: 10 * 1000 }, function () {

        /*browser.restart();

         browser.ignoreSynchronization=true*/
        //return console.log('Before Feature !!');
    });
    registerHandler("AfterFeature", { timeout: 10 * 1000 }, function () {
        browser.close();
         return browser.restart();
    });
    registerHandler("BeforeScenario", { timeout: 10 * 1000 }, function () {

        /*browser.restart();
         browser.ignoreSynchronization=true*/
        //return console.log('Before Scenario !!');
    });
    registerHandler("AfterScenario", { timeout: 10 * 1000 }, function () {
        //return console.log('After Scenario !!');
    });
});