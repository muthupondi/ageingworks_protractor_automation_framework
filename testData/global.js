console.log(process.env.TEST_ENV);
module.exports = {
    //TestingEnvironment: process.env.TEST_ENV,
    TestingEnvironment: 'QA',
    shortWait: 2000,
    longWait: 20000,

    urls:{
        AgeingWorks: {
            QA: 'http://usdf23v0218.mrshmc.com:5024/dashboard',
            UAT: 'xxxxxxxxxx',
            DEV: 'xxxxxxxxxx'
        }
    },

    users: {
        PrimaryUser: {
            QA: {
                username: 'test022.muthupondi.s@gisqa.mercer.com',
                password: 'Welcome2'
            },
            UAT: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            },
            PROD: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            }
        },
        HRProvisionedUser: {
            QA: {
                username: 'awqa101.eptest.admin2@gisqa.mercer.com',
                password: 'Welcome1',
            },
            UAT: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            },
            PROD: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            }
        },
        DependantUser: {
            QA: {
                username: 'test021.muthupondi.s@gisqa.mercer.com',
                password: 'Welcome1',
            },
            UAT: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            },
            PROD: {
                username: 'xxxxxxxxxxx',
                password: 'xxxxxxxxxxx'
            }
        }
    }

};
