
module.exports = {
    Login_Page: {
        input_Email: {
            selector: '//input[@type="email"]',
            locateStrategy: 'xpath'
        },
        input_Password: {
            selector: '//input[@type="password"]',
            locateStrategy: 'xpath'
        },
        link_AGlogo: {
            selector: '//img[@class="header__logo"]',
            locateStrategy: 'xpath'
        },
        btn_Submit: {
            selector: '//button[@type="submit"]',
            locateStrategy: 'xpath'
        },
        lbl_NewtoAgeingWorks: {
            selector: '//div[text()="New to AgeingWorks™?"]',
            locateStrategy: 'xpath'
        },
        lnk_Registerhere: {
            selector: '//a[@title="Register Here"]',
            locateStrategy: 'xpath'
        }
    },

    Registration_page: {
        link_AGlogo: {
            selector: '//img[@class="header__logo"]',
            locateStrategy: 'xpath'
        },
        Lbl_Personal_Info: {
            selector: '(//h5[@class="evo-type--header-rule evo-layout--margin-bottom"]/text())[2]',
            locateStrategy: 'xpath'
        },
        lbl_Firstname: {
            selector: '//strong[text()="First Name"]',
            locateStrategy: 'xpath'
        },
        input_Firstname: {
            selector: '//input[@formcontrolname="firstName"]',
            locateStrategy: 'xpath'
        },
        lbl_Lastname: {
            selector: '//*[text()="Last Name"]',
            locateStrategy: 'xpath'
        },

        input_Lastname: {
            selector: '//input[@formcontrolname="lastName"]',
            locateStrategy: 'xpath'
        },
        LLbl_Login_Info: {
            selector: '',
            locateStrategy: 'id'
        },

        input_Day: {
            selector: '(//div[@class="evo-dropdown "])[1]',
            locateStrategy: 'xpath'
        },
        input_Month: {
            selector: '(//div[@class="evo-dropdown "])[2]',
            locateStrategy: 'id'
        },
        input_Year: {
            selector: '(//div[@class="evo-dropdown "])[3]',
            locateStrategy: 'id'
        },
        drp_Gender: {
            selector: '//select[@formcontrolname="gender"]',
            locateStrategy: 'xpath'
        },
        drp_Genderdefault: {
            selector: '(//option[@selected])[0]',
            locateStrategy: 'xpath'
        },

        input_Companykeyword: {
            selector: '//input[@formcontrolname="companyKeyword"]',
            locateStrategy: 'xpath'
        },
        input_Emailaddress: {
            selector: '//input[@formcontrolname="email"]',
            locateStrategy: 'xpath'
        },

        input_RetypeEmailaddress: {
            selector: '//input[@formcontrolname="repeatEmail"]',
            locateStrategy: 'xpath'
        },
        input_RegPassword: {
            selector: '//input[@formcontrolname="password"]',
            locateStrategy: 'xpath'
        },
        input_RetypePassword: {
            selector: '//input[@formcontrolname="repeatPassword"]',
            locateStrategy: 'xpath'
        },
        btn_Save: {
            selector: '//button[@type="submit"]',
            locateStrategy: 'xpath'
        },
        btn_Cancel: {
            selector: '****',
            locateStrategy: 'id'
        },
        msg_Completefields: {
            selector: '//*[text()="Please complete all fields."]',
            locateStrategy: 'xpath'
        },
        msg_Header: {
            selector: '//*[@class="evo-layout--margin-top evo-layout--padding-bottom"]',
            locateStrategy: 'xpath'
        },
        lbl_Alreadyamember: {
            selector: '//div[contains(text(),"Already a Member?")]',
            locateStrategy: 'xpath'
        },
        lnk_Login: {
            selector: '//a[contains(text(),"Log in")]',
            locateStrategy: 'xpath'
        }
    },
    Home_Page: {
        lbl_Ageingworks: {
            selector: '//h1[text()="AgeingWorks™"]',
            locateStrategy: 'xpath'
        },
        btn_Gotoprofile: {
            selector: '//span[contains(.,"Go to profile")]',
            locateStrategy: 'xpath'
        },
        lnk_Healthmenu: {
            selector: '//a[contains(text(),"Health")]',
            locateStrategy: 'xpath'
        },
        lnk_Logout:{
            selector:'//a[@title="Log Out"]',
            locateStrategy:'xpath'
        },
        lnk_AcceptableUse: {
            selector: '//a[@title="Acceptable Use"]',
            locateStrategy: 'xpath'
        },
        lnk_Cookiepolicy: {
            selector: '//a[@title="Cookie Policy"]',
            locateStrategy: 'xpath'
        },
        lnk_Privacypolicy: {
            selector: '//a[@title="Privacy Policy"]',
            locateStrategy: 'xpath'
        },
        lnk_TermsofUse: {
            selector: '//a[@title="Terms of Use"]',
            locateStrategy: 'xpath'
        },
        lnk_Menu_Help_Contacts: {
            selector: '//span[text()="Help & Contacts"]',
            locateStrategy: 'xpath'
        },

        lnk_Help_Contacts: {
            selector: '//a[@id="footer_HJ3BW-xEtig-"]',
            locateStrategy: 'xpath'
        },
        lnk_Menu: {
            selector: '//i[@class="hrm-icon hrm-icon--menuIcon"]',
            locateStrategy: 'xpath'
        },
        lnk_DFC: {
            selector: '//span[text()="Digital Filing Cabinet"]',
            locateStrategy: 'xpath'
        },
        lnk_Profile: {
            selector: '//a[@title="Profile"]',
            locateStrategy: 'xpath'
        },
        lnk_Viewprofile: {
            selector: '//*[@title="View Profile"]',
            locateStrategy: 'xpath'
        },
        header_Notification_subc: {
            selector: '//h3[@class="evo-layout--no-margin evo-notification__title"]',
            locateStrategy: 'xpath'
        },
        msg_Notification_subc: {
            selector: '//div[@class="show-for-medium"]',
            locateStrategy: 'xpath'

        }
    },
    Articlesandvideos_Page: {
        icon_Bookmark: {
            selector: '(//i[@class="hrm-icon hrm-icon--marker_flag"])[1]',
            locateStrategy: 'xpath'
        },
        icon_Bookmarkflag: {
            selector: '(//i[@class="hrm-icon hrm-icon--marker_flag"])[1]',
            locateStrategy: 'xpath'
        },

        lbl_Bookmarkcount: {
            selector: '//a[@title="Bookmarks"]',
            locateStrategy: 'xpath'
        },

    },

    Footer_Page: {
        lnkheader_AcceptableUse: {
            selector: '(//a[contains(text(),"Acceptable Use")])[1]',
            locateStrategy: 'xpath'
        },
        lblheader_AcceptableUse: {
            selector: '//h1[text()="Acceptable Use"]',
            locateStrategy: 'xpath'
        },
        lblcontent_AcceptableUse: {
            selector: '//h3[text()="1. Prohibited uses"]',
            locateStrategy: 'xpath'
        },
        lnkheader_Cookiepolicy: {
            selector: '(//a[contains(text(),"Cookie Policy")])[1]',
            locateStrategy: 'xpath'
        },
        lblheader_Cookiepolicy: {
            selector: '//h1[text()="Cookie Policy"]',
            locateStrategy: 'xpath'
        },
        lblcontent_Cookiepolicy: {
            selector: '//h3[text()="1. WHAT IS A COOKIE?"]',
            locateStrategy: 'xpath'
        },
        lblheader_Privacypolicy: {
            selector: '//h1[text()="Privacy Policy"]',
            locateStrategy: 'xpath'
        },
        lblcontent_Privacypolicy: {
            selector: '//h1[text()="1. WHAT DATA DO WE  COLLECT?"]',
            locateStrategy: 'xpath'
        },
        lnkheader_TermsofUse: {
            selector: '(//a[contains(text(),"Terms of Use")])[1]',
            locateStrategy: 'xpath'
        },
        lblheader_TermsofUse: {
            selector: '//h1[text()="Terms of Use"]',
            locateStrategy: 'xpath'
        },
        lblcontent_TermsofUse: {
            selector: '//strong[text()="Other Applicable Terms"]',
            locateStrategy: 'xpath'
        },
        lnkheader_Help_Contacts: {
            selector: '(//a[contains(text(),"Help & Contacts")])[1]',
            locateStrategy: 'xpath'
        },
        lblheader_Help_Contacts: {
            selector: '//a[@id="evo-tab-group_ByvHZWlEtslZ"]',
            locateStrategy: 'xpath'
        },
        lblcontent_Help_Contacts: {
            selector: '//evo-accordion-static[text()="How do I search for information on the site?"]',
            locateStrategy: 'xpath'
        },


    },
    Health_page: {

        btn_Keepreading: {
            selector: '(//*[text()="Keep Reading"])[1]',
            locateStrategy: 'xpath'
        }
    },
    FullArticle_page: {
        lnk_Send: {
            selector: '//a[@title="Send"]',
            locateStrategy: 'xpath'

        },
        lbl_Sendtome: {
            selector: '//label[text()="Send to me"]',
            locateStrategy: 'xpath'
        },
        toggle_Sendtome: {
            selector: '//div[@class="evo-slide-toggle--slider"]',
            locateStrategy: 'xpath'
        },
        lbl_Selectexistingmember: {
            selector: '//label[text()="Select existing Member"]',
            locateStrategy: 'xpath'

        },

        drp_Selectexistingmember: {
            selector: '//select',
            locateStrategy: 'xpath'
        },

        lnk_nonmember: {
            selector: '//a[text()="Send to a non-member"]',
            locateStrategy: 'xpath'
        },
        lnk_member: {
            selector: '//a[text()="Send to a member"]',
            locateStrategy: 'xpath'
        },

        btn_Send: {
            selector: '//button[@title="Send"]',
            locateStrategy: 'xpath'
        },
        btn_Sendinactive: {
            selector: '//button[@title="Send" and @disabled]',
            locateStrategy: 'xpath'
        },
        lnk_Cancel: {
            selector: '//a[@title="Cancel"]',
            locateStrategy: 'xpath'
        },
        input_Firstname: {
            selector: '//input[@formcontrolname="firstName"]',
            locateStrategy: 'xpath'
        },
        input_Lastname: {
            selector: '//input[@formcontrolname="lastName"]',
            locateStrategy: 'xpath'
        },
        input_Emailaddress: {
            selector: '//input[@formcontrolname="email"]',
            locateStrategy: 'xpath'
        },
        lbl_Alertmsg: {
            selector: '//evo-alert-content[text()="Your Article has been sent."]',
            locateStrategy: 'xpath'
        },
        lnk_Close: {
            selector: '//a[@title="Close"]',
            locateStrategy: 'xpath'
        },
        lst_Promocard: {
            selector: '//ul[@evo-equalizer="header content"][1]/li',
            locateStrategy: 'xpath'
        }

    },
    Viewprofile_page: {
        lnk_communications: {
            selector: '//a[text()="Communications"]',
            locateStrategy: 'xpath'
        }
    },
    Communications_page: {
        header_Subscriptionpref: {
            selector: '//h3[text()="Subscription preferences"]',
            locateStrategy: 'xpath'
        },
        status_subscriptionpref: {
            selector: '(//div[@class="evo-flex-row--center-right"])[5]/div',
            locateStrategy: 'xpath'
        },
        header_Marketingpref: {
            selector: '//h3[text()="Marketing preferences"]',
            locateStrategy: 'xpath'
        },
        toggle_MarketingPrefNo: {
            selector: '//input[@class="evo-slide-toggle--input ng-untouched ng-pristine ng-valid"][1]',
            locateStrategy: 'xpath'
        },
        toggle_MarketingPrefYes: {
            selector: '//input[@class="evo-slide-toggle--input ng-untouched ng-valid ng-dirty"][1]',
            locateStrategy: 'xpath'
        },
        toggle_Phoneinactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue disabled"])[1]',
            locateStrategy:'xpath'
        },
        toggle_Emailinactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue disabled"])[2]',
            locateStrategy:'xpath'
        },
        toggle_Postinactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue"])[3]',
            locateStrategy:'xpath'
        },
        toggle_Phoneactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue"])[1]',
            locateStrategy:'xpath'
        },
        toggle_Emailactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue"])[2]',
            locateStrategy:'xpath'
        },
        toggle_Postactive:{
            selector:'(//div[@class="evo-slide-toggle evo-flex-row--center-left evo-slide-toggle--trans-blue"])[3]',
            locateStrategy:'xpath'
        },


        lbl_Marketingpref:{
            selector:'//span[contains(.,"Contact me with news, offers and suitable products")]',
            locateStrategy:'xpath'
        }

    },
    PersonalInfo_page: {
        header_Profileoverview: {
            selector: '//h3[contains(text(),"Profile Overview")]',
            locateStrategy: 'xpath'
        },
        txt_FirstName:{
            selector:'(//div[@class="profile__box__value"])[1]',
            locateStrategy:'xpath'
        },
        txt_LastName:{
            selector:'(//div[@class="profile__box__value"])[2]',
            locateStrategy:'xpath'
        },
        txt_Age:{
            selector:'(//div[@class="profile__box__value"])[3]',
            locateStrategy:'xpath'
        },
        txt_Sex:{
            selector:'(//div[@class="profile__box__value"])[4]',
            locateStrategy:'xpath'
        },
        txt_Address:{
            selector:'(//div[@class="profile__box__value"])[5]',
            locateStrategy:'xpath'
        },
        txt_PostCode:{
            selector:'(//div[@class="profile__box__value"])[6]',
            locateStrategy:'xpath'
        },
        txt_City:{
            selector:'(//div[@class="profile__box__value"])[7]',
            locateStrategy:'xpath'
        },
        txt_Country:{
            selector:'(//div[@class="profile__box__value"])[8]',
            locateStrategy:'xpath'
        },
        txt_Email:{
            selector:'(//div[@class="profile__box__value"])[9]',
            locateStrategy:'xpath'
        },
        txt_Phone:{
            selector:'(//div[@class="profile__box__value"])[10]',
            locateStrategy:'xpath'
        },
        txt_Mobile:{
            selector:'(//div[@class="profile__box__value"])[11]',
            locateStrategy:'xpath'
        },
        lst_Members: {
            selector: '//a[@title="<<membername>>"]',
            locateStrategy: 'xpath'
        },
        lnk_Edit: {
            selector: '//i[@class="evo-icon evo-icon--pen6"]',
            locateStrategy: 'xpath'

        },
        header_MyFamilyMembers: {
            selector: '//h3[contains(text(),"My Family Members and Connections")]',
            locateStrategy: 'xpath'
        },
        lnk_Adddependant: {
            selector: '//a[@title="undefined"]',
            locateStrategy: 'xpath'
        },
        checkbox_Confirminvite: {
            selector: '//label[@class="checkbox"]',
            locateStrategy: 'xpath'
        },
        btn_InviteInActive: {
            selector: '//button[@type="submit" and @disabled]',
            locateStrategy: 'xpath'
        },
        btn_Invite: {
            selector: '//button[contains(.,"Invite")]',
            locateStrategy: 'xpath'
        },

        lnk_Cancel: {
            selector: '//button[@class="evo-button evo-button--text evo-layout--float-right"]',
            locateStrategy: 'xpath'
        },


    },
    Dashboard_Page: {
        Header_Login: {
            selector: '//a[@id="headerlogin"]',
            locateStrategy: 'xpath'
        }
    },
    HelpandContacts_Page: {
        header_Form: {
            selector: '//h5[@class="evo-type--header-rule"]',
            locateStrategy: 'xpath'
        },
        drp_Feedback: {
            selector: '//select[@formcontrolname="type"]',
            locateStrategy: 'xpath'
        },
        input_Message: {
            selector: '//textarea[@placeholder="Leave us your message"]',
            locateStrategy: 'xpath'
        },
        btn_SendFeedback: {
            selector: '//button[@class="evo-button evo-button--standard"]',
            locateStrategy: 'xpath'
        },
        lbl_ErrorItemSelection: {
            selector: '(//div[@class="evo-feedback-form--box__error"])[1]',
            locateStrategy: 'xpath'
        },
        lbl_ErrorFeedbackRequired: {
            selector: '(//div[@class="evo-feedback-form--box__error"])[2]',
            locateStrategy: 'xpath'
        },
        lbl_SuccessMessage: {
            selector: "//*[contains(.,'Thank you for you feedback/query.') and @class='evo-alert__content']",
            locateStrategy: 'xpath'
        },
        header_text: {
            selector: '//h2[@class="title evo-text--sapphire-dark highlight-heade"]',
            locateStrategy: 'xpath'
        },
        lbl_CountryPhoneNo:{
            selector:'(//a[contains(.,"UK: 0800 000 0000")])[2]',
            locateStrategy:'xpath'
        },
        lbl_CountryEmail:{
            selector:'(//a[contains(.,"support@ageingworks.com")])[2]',
            locateStrategy:'xpath'
        }


    },
    TermsAndConditions_Page: {
        header_TermsOfUse: {
            selector:'//h1[text()="Terms of Use"]',
            locateStrategy: 'xpath'
        },
        header_General:{
            selector:'//h3[contains(text(),"1. General")]',
            locateStrategy:'xpath'
        },
        checkbox_AcceptNotChecked:{
            selector:'//input[@class="evo-text--primary-color background--none evo-tooltip--host ng-untouched ng-pristine ng-valid"]',
            locateStrategy:'xpath'
        },
        msg_Error: {
            selector:'(//*[contains(.,"OOPS! Please accept the Terms of Use. Thank you!")])[4]',
            locateStrategy:'xpath'
        },

        checkbox_AcceptChecked:{
            selector:'//input[@class="evo-text--primary-color background--none evo-tooltip--host ng-valid ng-dirty ng-touched"]',
            locateStrategy:'xpath'
        },
        lbl_Accept:{
            selector:'//label[@class="checkbox"]',
            locateStrategy:'xpath'
        },
        btn_Agreedisabled:{
            selector:'//button[@class="evo-button evo-button--standard adobe-terms-conditions evo-button--disabled"]',
            locateStrategy:'xpath'
        },

        btn_Agreeenabled:{
            selector:'//button[@class="evo-button evo-button--standard adobe-terms-conditions"]',
            locateStrategy:'xpath'
        },
        modalheader_TermsOfUse:{
            selector:'//evo-modal-title[contains(.,"Terms of Use")]',
            locateStrategy:'xpath'
        },
        btn_Dismiss:{
            selector:'//button[@class="evo-button evo-button--standard evo-button--wide-lg"]',
            locateStrategy:'xpath'
        },
        lnk_close:{
            selector:'//i[@class="evo-icon evo-icon--cross2 evo-modal__close"]',
            locateStrategy:'xpath'
        },
        lnk_Disagree:{
            selector:'//button[contains(text(),"Disagree")]',
            locateStrategy:'xpath'
        }



    }
}


